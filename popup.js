var jiraFormatHeader = '||Name||ParentObject||Type||APIName||\n';
var googleDocsFormatHeader = 'Name\tParentObject\tType\tAPIName\n';
var jiraFormat = "";
var googleDocsFormat = "";
var jiraFormatClipboard = "";
var googleDocsFormatClipboard = "";

// Inject the payload.js script into the current tab after the popout has loaded
window.addEventListener('load', function (evt) {
    chrome.extension.getBackgroundPage().chrome.tabs.executeScript(null, {
        file: 'payload.js'
    });;
});

// Listen to messages from the payload.js script and write to popout.html
chrome.runtime.onMessage.addListener(function (message) {
    if(message.length > 0){
        var tbody = document.getElementById('tbody');
        for (var i = 0; i < message.length; i++) {
            var tr = "<tr>";
            tr += "<td>" + message[i].Name + "</td>";
            tr += "<td>" + message[i].ParentObject + "</td>";
            tr += "<td>" + message[i].Type + "</td>";
            tr += "<td>" + message[i].APIName + "</td>";
            tr += "</tr>";

            tbody.innerHTML += tr;

            jiraFormat += "|"

            jiraFormat += (message[i].Name === "") ? " " : message[i].Name;
            jiraFormat += "|";
            jiraFormat += (message[i].ParentObject === "") ? " " : message[i].ParentObject;
            jiraFormat += "|";
            jiraFormat += (message[i].Type === "") ? " " : message[i].Type;
            jiraFormat += "|";
            jiraFormat += (message[i].APIName === "") ? " " : message[i].APIName;
            jiraFormat += "|\n";

            googleDocsFormat += message[i].Name + "\t";
            googleDocsFormat += message[i].ParentObject + "\t";
            googleDocsFormat += message[i].Type + "\t";
            googleDocsFormat += message[i].APIName + "\n";
        }
        jiraFormatClipboard = jiraFormatHeader + jiraFormat;
        googleDocsFormatClipboard = googleDocsFormatHeader + googleDocsFormat;;

        bindClipboardDataButtons();

        document.getElementById("show-header-row").addEventListener('change', function() {
            if(this.checked) {
                jiraFormatClipboard = jiraFormatHeader + jiraFormat;
                googleDocsFormatClipboard = googleDocsFormatHeader + googleDocsFormat;
                document.getElementById("header-row").style.visibility = "visible";
            } else {
                jiraFormatClipboard = jiraFormat;
                googleDocsFormatClipboard = googleDocsFormat;
                document.getElementById("header-row").style.visibility = "collapse";
            }
            bindClipboardDataButtons();
        });
    } else {
        document.getElementById("no-deployment-notes").style.display = "block";
        document.getElementById("deployment-notes-table").style.display = "none";
    }
});

function bindClipboardDataButtons (){
    var clipboard = {
        data      : '',
        intercept : false,
        hook      : function (evt){
            if (clipboard.intercept) {
                evt.preventDefault();
                evt.clipboardData.setData('text/plain', clipboard.data);
                clipboard.intercept = false;
                clipboard.data = '';
            }
        }
    };
    window.addEventListener('copy', clipboard.hook);

    document.getElementById('jiraFormatButton').addEventListener('click', function () {
        clipboard.data = jiraFormatClipboard;
        if (window.clipboardData) {
            window.clipboardData.setData('Text', clipboard.data);
        } else {
            clipboard.intercept = true;
            document.execCommand('copy');
        }
    });
    document.getElementById('googleDocsFormatButton').addEventListener('click', function () {
        clipboard.data = googleDocsFormatClipboard;
        if (window.clipboardData) {
            window.clipboardData.setData('Text', clipboard.data);
        } else {
            clipboard.intercept = true;
            document.execCommand('copy');
        }
    });
}
