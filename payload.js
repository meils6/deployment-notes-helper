
var deploymentNotesObjectList = [];

var table = document.getElementById('outboundChangeSetDetailPage:outboundChangeSetDetailPageBody:outboundChangeSetDetailPageBody:detail_form:outboundCs_componentsBlock:OutboundChangeSetComponentBlockSection:OutboundChangeSetComponentList:tb');
if(table){
    for (var i = 0, row; row = table.rows[i]; i++) {
        var rowObject = {};
        rowObject.Name = row.cells[1].childNodes[0].innerHTML;
        rowObject.ParentObject = row.cells[2].innerHTML;
        rowObject.Type = row.cells[3].innerHTML;
        rowObject.APIName = row.cells[4].innerHTML;
        deploymentNotesObjectList.push(rowObject);
    }
}

var elements = document.getElementsByClassName('file');
if(elements){
    var componentNameMap = { cls: "Apex Class", page: "Visualforce Page", component: "Visualforce Component", trigger: "Apex Trigger" }

    for (var i = 0; i < elements.length; i++){
        var fileIdentifier = elements[i].getAttribute('data-file-identifier');
        if(fileIdentifier){
            var fileNameAndType = fileIdentifier.split('/')[2];
            var filetype = fileNameAndType.split('.')[1];
            var fileName = fileNameAndType.split('.')[0];

            if(! filetype.includes('meta')){
                var rowObject = {};
                rowObject.Name = fileName;
                rowObject.ParentObject = '';
                rowObject.Type = componentNameMap[filetype];
                rowObject.APIName = fileName;
                deploymentNotesObjectList.push(rowObject);
            }
        }
    }
}
// send the list as a chrome message
chrome.runtime.sendMessage(deploymentNotesObjectList);
