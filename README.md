Deployment notes helper
=======
A browser extension which creates formatted deployment notes from a bitbucket pull request or a salesforce change set

## Installation:
* Download the code
* Navigate chrome to `chrome://extensions`
* Check the `Developer mode` toggle
* Click on `Load Unpacked Extension...`
* Select the folder containing the extension
